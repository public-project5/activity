<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Временный роут для быстрого создания токена
 * TODO: при необходимости удалить
 */
Route::get('/tokens/create/092e8cb3109ec1n0cijc1-ws0ixn19dhbx93bdgs8hb', static function () {
    $credentials = [
        'name' => 'landing',
        'email' => 'test@landing',
        'password' => 'oskjchbqowihcdjkschkajxch',
    ];

    if (!$user = \App\Models\User::where('email', $credentials['email'])->where(
        'password',
        $credentials['password']
    )->first()) {
        $user = new \App\Models\User($credentials);
        $user->save();
    }

    $token = $user->createToken('landing-api');
    return ['token' => $token->plainTextToken];
});

