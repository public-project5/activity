<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('', function () {
    return abort(401);
})->name('login');

Route::middleware('auth:sanctum')->prefix('/v1/user')->group(function () {
    Route::rpc('/activity', [\App\Http\Procedures\UserActivityProcedure::class])->name('rpc.user.activity');
});
