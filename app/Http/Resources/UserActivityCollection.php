<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserActivityCollection extends ResourceCollection
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_LIMIT = 10;
    const DEFAULT_GROUP_BY = 'url';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = $request->validate([
            'page' => 'numeric',
            'limit' => 'numeric'
        ]);

        $page = $data['page'] ?? self::DEFAULT_PAGE;
        $limit = $data['limit'] ?? self::DEFAULT_LIMIT;

        $userActivities = $this->collection->sortBy(function ($v) {
            return (new Carbon())->setTimeFromTimeString($v->datetime)->valueOf();
        }, SORT_NUMERIC, true)->groupBy(self::DEFAULT_GROUP_BY);

        $items = [];
        foreach ($userActivities as $key => $userActivity) {
            $items[] = [
                'url' => $key,
                'count' => count($userActivity),
                'last_visit' => $userActivity[0]['datetime'],
            ];
        }

        usort($items, function ($c, $n) {
            return $c['count'] < $n['count'];
        });

        return [
            'items' => array_slice($items, ($page - 1) * $limit, $limit),
            'page' => $page,
            'limit' => $limit,
            'total' => count($items),
        ];
    }
}
