<?php

declare(strict_types = 1);

namespace App\Http\Procedures;

use App\Http\Resources\UserActivityCollection;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Sajya\Server\Procedure;

class UserActivityProcedure extends Procedure
{
    /**
     * The name of the procedure that will be
     * displayed and taken into account in the search
     *
     * @var string
     */
    public static string $name = 'UserActivity';

    /**
     * добавляет запись об активности пользователя
     * @param Request $request
     *
     * @return array|string|integer
     */

    public function add(Request $request)
    {
        $data = $request->validate([
            'url' => 'required|max:2048|URL',
            'datetime' => 'required|date_format:d.m.Y h:i:s',
        ]);

        $userActivity = new UserActivity($data);
        $userActivity->save();

        return ['status' => 'success'];
    }

    /**
     * возвращает подготовленный список активностей пользователей с пагинацией
     * @param Request $request
     * @return UserActivityCollection
     */
    public function get(Request $request)
    {
        return new UserActivityCollection(UserActivity::all());
    }
}
