<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'datetime',
    ];

    /**
     * @param string|\DateTimeInterface $value
     * @return void
     */
    public function setDatetimeAttribute(string|\DateTimeInterface $value) : void
    {
        $this->attributes['datetime'] = (new Carbon($value))->toDateTime();
    }

    public function toArray()
    {
        return [
            'id' => $this->getAttributeValue('id'),
            'url' => $this->getAttributeValue('url'),
            'visit_at' => (new Carbon($this->getAttributeValue('datetime')))->format('d.m.Y h:i:s'),
        ];
    }


}
