<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## Установка
после клонирования устанавливаем sail и запускаем контейнеры в режиме демона:
````
cd ./activity
cp .env.example .env
docker run --rm --interactive --tty -v $(pwd):/app composer install
./vendor/bin/sail up -d
````

после запуска устанавливаем миграции:
````
./vendor/bin/sail artisan migrate
````

чтобы получить токен нужно открыть в браузере ссылку:

http://localhost:81/tokens/create/092e8cb3109ec1n0cijc1-ws0ixn19dhbx93bdgs8hb
